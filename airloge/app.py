# MIT License
#
# Copyright (c) 2020 Emmanuel Thomas
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""

Airloge - Your flight watch

"""

from os import path
from os import remove
from os import stat
import time
from datetime import date
from threading import Lock
import configparser
import logging
import pickle

from tinydb import TinyDB
from tinydb.storages import MemoryStorage
from timeloop import Timeloop
from geographiclib.geodesic import Geodesic
from microdotphat import clear, write_string, show
import wget
import pandas

import airloge.helpers as helpers

class Airloge:
    '''
    Airloge app.
    '''

    def __init__(self):
        self.os_aircraft_url = 'https://opensky-network.org/datasets/metadata/aircraftDatabase.csv'
        self.os_csv_file = './aircraftDatabase.csv'
        self.os_pickled_file = './os_pandas.obj'
        self.os_data = None
        self.timeloop = Timeloop()
        self.conf = configparser.ConfigParser()
        self.geod = Geodesic.WGS84
        self.database = TinyDB(storage=MemoryStorage)
        self.database.purge_tables()
        self.db_lock = Lock()
        self.aircrafts = self.database.table('aircrafts')
        self.control_zone = [0.0, 0.0]
        self.control_zone_radius = 0.0
        self.refresh_rate = 0

    def init(self):
        '''
        Initialise app.
        '''
        logging.getLogger().setLevel(logging.INFO)
        logging.info("Airloge --- Welcome onboard !")
        logging.info("Initialisation ...")
        self.conf.read('config.ini')
        self.conf.sections()
        self.control_zone = [
            self.conf['DEFAULT'].getfloat('latitude'),
            self.conf['DEFAULT'].getfloat('longitude')]
        self.control_zone_radius = self.conf['DEFAULT'].getfloat('radius')
        self.refresh_rate = self.conf['DEFAULT'].getint('refresh_rate') # in seconds. Minimum 10.
        self.init_aircraft_database()
        logging.info("Initialisation done")

    def run(self):
        '''
        Run the Airloge.
        '''
        self.timeloop.start()

        microdot = False
        try:
            #Test if microdot is installed and connected
            import smbus #pylint:disable=import-outside-toplevel
            smbus.SMBus(1)
            microdot = True
        except FileNotFoundError as err:
            logging.exception(err)
            logging.info('Microdot not installed, continuing without the microdot')
        except PermissionError as err:
            logging.exception(err)
            logging.info('Microdot not found, continuing without the microdot')

        while True:
            try:
                if microdot:
                    self.display_info()
                else:
                    time.sleep(1)
            except KeyboardInterrupt:
                self.timeloop.stop()
                break

    def display_info(self):
        '''
        Display info.
        '''
        with self.db_lock:
            aircrafts = [a for a in self.aircrafts.all()
                         if helpers.distance(a) <= self.control_zone_radius]
        aircrafts.sort(key=helpers.distance)
        for aircraft in aircrafts:
            clear()
            show()
            time.sleep(0.5)
            # 1. Show callsign
            write_string(helpers.callsign(aircraft, 'UFO007'), kerning=False)
            show()
            time.sleep(2)
            # 2. Show model type code
            write_string(' ' + helpers.typecode(aircraft, 'UND'), kerning=False)
            show()
            time.sleep(2)
            # 3. Show country of registration
            write_string(' ' + helpers.country(aircraft, 'ZZZ')[0], kerning=False)
            show()
            time.sleep(2)
            aircraft_distance = helpers.distance(aircraft)
            if aircraft_distance < 1000:
                distance_text = '{}m'.format(round(aircraft_distance))
            elif aircraft_distance > 100000:
                distance_text = '{}km'.format(round(aircraft_distance / 1000))
            else:
                distance_text = '{}km'.format(round(aircraft_distance / 1000, 1))

            # 4. Show distance from control zone's center
            write_string(distance_text, kerning=False)
            show()
            time.sleep(2)

    def init_aircraft_database(self):
        '''
        Initialise the aircraft database.
        '''
        to_refresh = False
        if path.exists(self.os_pickled_file):
            last_time_updated = stat(self.os_pickled_file).st_mtime
            elapsed = date.today() - date.fromtimestamp(last_time_updated)
            if elapsed.days > 6:
                remove(self.os_pickled_file)
                to_refresh = True
        else:
            to_refresh = True

        if to_refresh:
            if path.exists(self.os_csv_file):
                remove(self.os_csv_file)
            logging.info("Downloading OS aircraft database ...")
            wget.download(self.os_aircraft_url, self.os_csv_file)
            logging.info("Downloading OS aircraft database done.")
            self.os_data = pandas.read_csv(
                self.os_csv_file, sep=',', index_col=0,
                usecols=['icao24', 'model', 'typecode', 'owner'],
                dtype={
                    'icao24': 'string', 'model': 'string', 'typecode': 'string',
                    'owner': 'string'})
            remove(self.os_csv_file)
            with open(self.os_pickled_file, 'wb') as os_pickled:
                pickle.dump(self.os_data, os_pickled)
        else:
            with open(self.os_pickled_file, 'rb') as os_pickled:
                self.os_data = pickle.load(os_pickled)

    def get_aircraft_info(self, icao24):
        '''
        Get information about the aircraft identified by the icao24 given as input.
        '''
        info = {}
        try:
            info = self.os_data.loc[icao24]
        except KeyError:
            logging.warning("Aircraft %s not found in OS database.", icao24)

        return info

    def get_pmin(self, radius):
        '''
        Calculate the "bottom-left" point of a region whose center is the control zone center.
        '''
        return self.geod.Direct(self.control_zone[0], self.control_zone[1], 225, radius)

    def get_pmax(self, radius):
        '''
        Calculate the "top-right" point of a region whose center is the control zone center.
        '''
        return self.geod.Direct(self.control_zone[0], self.control_zone[1], 45, radius)

    def get_geodesic_inverse(self, latitude, longitude):
        '''
        Calculate the distance between the center of the control zone and the position
        of the point given by latitude - longitude.
        '''
        return self.geod.Inverse(
            self.control_zone[0], self.control_zone[1],
            latitude, longitude)
