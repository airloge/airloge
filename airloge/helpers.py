# MIT License
#
# Copyright (c) 2020 Emmanuel Thomas
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""

Airloge - Your flight watch

"""

import logging

import pandas
from pycountry import countries

def distance(state):
    '''
    Return the distance from an aircraft state.
    '''
    return state['geod']['s12']

def model(aircraft, default):
    '''
    Return the model of an aircraft object.
    '''
    return (aircraft['info']['model']
            if ('model' in aircraft['info'] and not pandas.isna(aircraft['info']['model']))
            else default)

def typecode(aircraft, default):
    '''
    Return the typecode of the model of an aircraft object, e.g. B737.
    '''
    return (aircraft['info']['typecode']
            if ('typecode' in aircraft['info'] and not pandas.isna(aircraft['info']['typecode']))
            else default)

def owner(aircraft, default):
    '''
    Return the owner of an aircraft object.
    '''
    return (aircraft['info']['owner']
            if ('owner' in aircraft['info'] and not pandas.isna(aircraft['info']['owner']))
            else default)

def callsign(aircraft, default):
    '''
    Return the callsign of an aircraft object.
    '''
    return default if aircraft['state'][1] == "" else aircraft['state'][1]

def country(aircraft, code_default):
    '''
    Return the alpha-3 iso code and the full name of the country of registration
    of an aircraft object.
    '''
    code = code_default
    try:
        code = countries.lookup(aircraft['state'][2]).alpha_3
    except LookupError as err1:
        # Try to reshuffle the name
        variant = aircraft['state'][2].lower()
        if variant.startswith('republic of'):
            variant = variant.replace('republic of', '').strip() + ', republic of'
            try:
                code = countries.lookup(variant).alpha_3
            except LookupError as err2:
                logging.exception(err1)
                logging.exception(err2)
        else:
            logging.exception(err1)
    return code, aircraft['state'][2]
