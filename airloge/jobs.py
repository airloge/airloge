# MIT License
#
# Copyright (c) 2020 Emmanuel Thomas
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""

Airloge - Your flight watch

Jobs executed periodically by Airloge.

"""

import time
from datetime import timedelta
import logging

import requests
from tinydb import Query

from airloge import APP
from airloge import helpers

@APP.timeloop.job(interval=timedelta(days=7))
def refresh_aircraft_database():
    '''
    Refresh the aircraft database.
    '''
    APP.init_aircraft_database()

@APP.timeloop.job(interval=timedelta(seconds=APP.refresh_rate))
def fetch_flyingover_aircrafts():
    '''
    Fetch flying over aircrafts.
    '''
    if APP.refresh_rate < 10:
        raise Exception("Please set a refresh rate greater or equal than 10s.")
    try:
        query_radius = APP.control_zone_radius + 2e3
        pmin = APP.get_pmin(query_radius)
        pmax = APP.get_pmax(query_radius)
        opensky_response = requests.get((
            'https://opensky-network.org/api/states/all'
            '?lamin={}&lomin={}&lamax={}&lomax={}').format(
                pmin['lat2'], pmin['lon2'], pmax['lat2'], pmax['lon2']))

        if opensky_response.status_code == 200:
            data = opensky_response.json()
            aircraft = Query()
            if 'states' in data and data['states']:
                states = [s for s in data['states'] if not s[8]] # flying aircrafts
                for state in states:
                    info = APP.get_aircraft_info(state[0])
                    try:
                        with APP.db_lock:
                            APP.aircrafts.upsert(
                                {
                                    'icao24': state[0],
                                    'state': state,
                                    'geod': APP.get_geodesic_inverse(state[6], state[5]),
                                    'info': info
                                },
                                aircraft.icao24 == state[0])
                    except ValueError as err:
                        logging.exception(err)
        else:
            logging.warning("OS API responded with %d", opensky_response.status_code)
    except ValueError as err:
        logging.exception(err)
    except requests.exceptions.RequestException as err:
        logging.exception(err)


@APP.timeloop.job(interval=timedelta(seconds=APP.refresh_rate * 2))
def cleanup_flyingover_aircrafts():
    '''
    Clean up the list of retrieved flying over aircrafts.
    '''
    aircraft = Query()
    with APP.db_lock:
        APP.aircrafts.remove(
            (aircraft.geod['s12'] > APP.control_zone_radius) | # out of the zone
            (aircraft.state[4] < (time.time() - APP.refresh_rate * 3)) | # old state
            (aircraft.state[8])) # is on the ground

@APP.timeloop.job(interval=timedelta(seconds=APP.refresh_rate))
def print_flyingover_aircrafts():
    '''
    Print flying over aircrafts.
    '''
    with APP.db_lock:
        aircrafts = [a for a in APP.aircrafts.all()
                     if helpers.distance(a) <= APP.control_zone_radius]
    aircrafts.sort(key=helpers.distance)
    for aircraft in aircrafts:
        print('{}, {} ({}) owned by {} ({}) at {:.3f}km'.format(
            helpers.callsign(aircraft, 'no callsign'),
            helpers.model(aircraft, 'some model'),
            helpers.typecode(aircraft, 'UND'),
            helpers.owner(aircraft, 'someone'),
            helpers.country(aircraft, 'ZZZ')[0],
            helpers.distance(aircraft)/1000))

    print("")
