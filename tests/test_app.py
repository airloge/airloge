#!/usr/bin/env python3

# MIT License
#
# Copyright (c) 2020 Emmanuel Thomas
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""

Airloge - Your flight watch

"""

import unittest
from os import path

from context import airloge

class TestAppInit(unittest.TestCase):
    '''
    Test the initialisation of the Airloge app.
    '''

    def setUp(self):
        self.app = airloge.Airloge()

    def test_fresh_start(self):
        '''
        Test conditions of a fresh start.
        '''
        self.assertFalse(path.exists(self.app.os_pickled_file))

    def test_init(self):
        '''
        Test init.
        '''
        self.app.init()
        self.assertTrue(path.exists(self.app.os_pickled_file))
