# Airloge - Your flight watch

![demo](/assets/airloge.gif "Airloge demo")

Set a GPS coordinate of your control zone and it will tell you what aircrafts
are flying in your zone.

It has been developped to be used on a Raspberry Pi W Zero mounted with a
microdot phat matrix display.

If no microdot phat is detected, you still have the aircraft information being
output on the standard output.

## Prerequisites

- Software
  - python 3.6+
  - pip3
- Hardware
  - 40-pin header Raspberry Pi models, e.g. [Raspberry Pi Zero W](https://www.raspberrypi.org/products/raspberry-pi-zero-w/)
  - [Micro Dot pHAT](https://shop.pimoroni.com/products/microdot-phat?variant=25454635591)

If Airloge doesn't detect the Micro Dot pHAT, it still runs and prints
the aircraft informations on the standard output. 

## Installation

### From source

You may also use a Python virtual environment if you wish.

```shell
git clone https://gitlab.com/airloge/airloge.git
cd airloge
pip3 install -r requirements.txt
```

## Configuration

Copy `config.ini.example` to `config.ini`.

Change the parameters in `config.ini` to define your control zone,
i.e. `longitude`, `latitude` and `radius`.

## Usage

Run the command:

```shell
python3 -m airloge
```
## Data provider

The live airspace information is retrieved from [The OpenSky Network API](https://opensky-network.org/apidoc/)
provided by [OpenSky Network](https://opensky-network.org).

## License

See [LICENSE](LICENSE)
